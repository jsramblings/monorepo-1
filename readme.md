# Monorepo

* two react apps
* one shared package for design system

Goals
* share the design system between the two apps
* be able to develop as usual, no overhead of switching between repos or having to build to see changes
* be able to publish the design system as its own package, if needed 
* be able to publish and release the two apps separately
